## Description


## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Lint

```bash
$ yarn lint
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov

$ test:watch

$ test:debug
```

## Migrations

```bash
$ yarn migration:create

$ yarn migration:run

$ yarn migration:revert

$ query "SQL QUERY"
```