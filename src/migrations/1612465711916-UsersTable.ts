import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class UsersTable1612465711916 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')

      await queryRunner.createTable(new Table({
        name: 'users',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'email',
            type: 'varchar',
            isUnique: true,
            isNullable: false
          },
          {
            name: 'phone',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'password',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'active',
            type: 'bool',
            isNullable: false
          },
          {
            name: 'role',
            type: 'integer',
            isNullable: false
          },
          {
            name: 'verified',
            type: 'bool',
            isNullable: false
          },
          {
            name: 'createdAt',
            type: 'date',
            isNullable: false,
            default: 'NOW()'
          },
          {
            name: 'updatedAt',
            type: 'date',
            isNullable: false,
            default: 'NOW()'
          }
        ]
      }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('users')
      await queryRunner.query('DROP EXTENSION "uuid-ossp"')
    }

}
