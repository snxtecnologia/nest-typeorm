import { CanActivate, Injectable } from '@nestjs/common'
import { Observable } from 'rxjs'
import authConstants from '../components/auth/auth-constants'
import { AuthService } from '../components/auth/auth.service'

@Injectable()
export class WsGuard implements CanActivate {

  constructor(private authService: AuthService) {
  }

  canActivate(
    context: any,
  ): boolean | any | Promise<boolean | any> | Observable<boolean | any> {
    const bearerToken = context.args[0].handshake.headers.authorization.split(' ')[1]
    try {
      return new Promise((resolve, reject) => {
        return this.authService.verifyToken(bearerToken, authConstants.jwt.secrets.accessToken)
          .then(
            (user) => {
              if (user) {
                resolve(user)
              } else {
                reject(false)
              }
            }
          )
      })
    } catch (ex) {
      console.log(ex)
      return false
    }
  }
}