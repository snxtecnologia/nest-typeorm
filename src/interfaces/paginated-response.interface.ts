import { PaginatedRequest } from './paginated-request.interface'

export class PaginatedResponse<T> {
  items: Array<T>
  metadata: Partial<PaginatedRequest>
}