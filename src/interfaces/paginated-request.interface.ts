export class PaginatedRequest {
  count: number
  limit: number = 10
  offset: number = 0
  totalPages: number
  orderDir: string = 'asc'
  orderBy: string = 'id'
  search: any

  public get order() {
    const dir = this.orderDir === 'asc' ? 'ASC' : 'DESC'
    return [{ key: String(this.orderBy || 'id'), value: dir }].reduce((map: any, obj: any) => {
      map[obj.key] = obj.value
      return map
    }, {})
  }
}