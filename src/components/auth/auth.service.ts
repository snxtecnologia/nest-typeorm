import { Injectable, NotFoundException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { CreateUserDto } from '../user/dto/create-user.dto'
import { UserService } from '../user/user.service'
import { ValidateUserOutput } from './interfaces/validate-user-output.interface'
import { LoginPayload } from './interfaces/login-payload.interface'
import authConstants from './auth-constants'
import { DecodedUser } from './interfaces/decoded-user.interface'
import AuthRepository from './auth.repository'
import JwtTokensDto from './dto/jwt-tokens.dto'
// import { UpdateUserDto } from '../user/dto/update-user.dto'

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly authRepository: AuthRepository,
  ) { }

  public async validateUser(email: string, password: string): Promise<null | ValidateUserOutput> {
    const user = await this.userService.findByEmail(email)

    if (!user) {
      throw new NotFoundException('The item does not exist')
    }

    // if(true){
    //   const passw = this.encryptPassword('Admin@123')
    //   this.userService.update(user.id, Object.assign(new UpdateUserDto(), { password: passw }))
    // }

    const passwordCompared = this.userService.validatePassword(password, user.password)

    if (passwordCompared) {
      return {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
      }
    }

    return null
  }

  public async signin(data: LoginPayload): Promise<JwtTokensDto> {
    const payload: LoginPayload = {
      id: data.id,
      name: data.name,
      email: data.email,
      role: data.role,
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: authConstants.jwt.expirationTime.accessToken,
      secret: authConstants.jwt.secrets.accessToken,
    })
    const refreshToken = this.jwtService.sign(payload, {
      expiresIn: authConstants.jwt.expirationTime.refreshToken,
      secret: authConstants.jwt.secrets.refreshToken,
    })

    await this.authRepository.addRefreshToken(
      payload.email as string,
      refreshToken,
    )

    return {
      accessToken,
      refreshToken
    }
  }

  public createVerifyToken(id: number): string {
    return this.jwtService.sign(
      { id },
      {
        expiresIn: authConstants.jwt.expirationTime.accessToken,
        secret: authConstants.jwt.secrets.accessToken,
      },
    )
  }

  getRefreshTokenByEmail(email: string): Promise<string | null> {
    return this.authRepository.getToken(email)
  }

  deleteTokenByEmail(email: string): Promise<number> {
    return this.authRepository.removeToken(email)
  }

  deleteAllTokens(): Promise<string> {
    return this.authRepository.removeAllTokens()
  }

  public verifyEmailVerToken(token: string, secret: string) {
    return this.jwtService.verifyAsync(token, { secret })
  }

  public async signup(signup: CreateUserDto): Promise<any> {
    return this.userService.create(signup)
  }

  public verifyTokenSync(token: string, secret: string): DecodedUser | null {
    return (this.jwtService.verify(token, { secret })) as DecodedUser | null
  }

  public verifyToken(token: string, secret: string): Promise<DecodedUser | null> {
    return this.jwtService.verifyAsync(token, { secret })
      .then((user) => {
        return user as DecodedUser | null
      }).catch(
        () => {
          return null
        }
      )
  }
}
