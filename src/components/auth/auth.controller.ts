import {
  Body,
  Controller,
  HttpCode,
  Get,
  Post,
  Delete,
  Param,
  Request,
  UnauthorizedException,
  UseGuards,
  NotFoundException,
  ForbiddenException,
  HttpStatus,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
  NotImplementedException,
} from '@nestjs/common'
import {
  ApiTags,
  ApiBody,
  ApiOkResponse,
  ApiInternalServerErrorResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiNoContentResponse,
  ApiExtraModels,
  getSchemaPath,
  ApiForbiddenResponse,
} from '@nestjs/swagger'
import { JwtService } from '@nestjs/jwt'
import { Request as ExpressRequest } from 'express'


import authConstants from './auth-constants'
import { DecodedUser } from './interfaces/decoded-user.interface'
import LocalAuthGuard from './guards/local-auth.guard'
import RefreshTokenDto from './dto/refresh-token.dto'
import SignInDto from './dto/sign-in.dto'
import JwtTokensDto from './dto/jwt-tokens.dto'
import WrapResponseInterceptor from '../../interceptors/wrap-response.interceptor'
import { AuthService } from './auth.service'
import { UserService } from '../user/user.service'
import { User } from '../user/entities/user.entity'
import { CreateUserDto } from '../user/dto/create-user.dto'
import JwtAccessGuard from '../../guards/jwt-access.guard'
import RolesGuard from '../../guards/roles.guard'
import { Roles, RolesEnum } from '../../decorators/roles.decorator'
import AuthBearer from '../../decorators/auth-bearer.decorator'

@ApiTags('Authentication')
@UseInterceptors(WrapResponseInterceptor)
@ApiExtraModels(JwtTokensDto)
@Controller('api/auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) { }

  @ApiBody({ type: SignInDto })
  @ApiOkResponse({
    schema: {
      type: 'object',
      properties: {
        data: {
          $ref: getSchemaPath(JwtTokensDto),
        },
      },
    },
    description: 'Returns jwt tokens',
  })
  @ApiBadRequestResponse({
    schema: {
      type: 'object',
      example: {
        message: [
          {
            target: {
              email: 'string',
              password: 'string',
            },
            value: 'string',
            property: 'string',
            children: [],
            constraints: {},
          },
        ],
        error: 'Bad Request',
      },
    },
    description: '400. ValidationException',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiBearerAuth()
  @HttpCode(HttpStatus.OK)
  @UseGuards(LocalAuthGuard)
  @Post('sign-in')
  async signIn(@Request() req: ExpressRequest): Promise<JwtTokensDto> {
    const { password, ...user } = req.user as SignInDto

    return this.authService.signin(user)
  }

  @ApiBody({ type: CreateUserDto })
  @ApiOkResponse({
    description: '201, Success',
  })
  @ApiBadRequestResponse({
    schema: {
      type: 'object',
      example: {
        message: [
          {
            target: {
              email: 'string',
              password: 'string',
            },
            value: 'string',
            property: 'string',
            children: [],
            constraints: {},
          },
        ],
        error: 'Bad Request',
      },
    },
    description: '400. ValidationException',
  })
  @ApiConflictResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
      },
    },
    description: '409. ConflictResponse',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post('sign-up')
  @UsePipes(new ValidationPipe({ transform: true }))
  async signUp(@Body() user: CreateUserDto): Promise<any> {
    throw new NotImplementedException('Cant signup')
    // const { id, email } = await this.userService.create(user)
    // const token = this.authService.createVerifyToken(id)

    // await this.sendGrid.send({
    //   to: email,
    //   from: process.env.MAILER_FROM_EMAIL,
    //   templateId: authConstants.mailer.verifyEmail.template,
    //   dynamicTemplateData: {
    //     subject: authConstants.mailer.verifyEmail.subject,
    //     token,
    //     email,
    //     host: process.env.SERVER_HOST,
    //   },
    // })

    return { message: 'Success! please verify your email' }
  }

  @ApiOkResponse({
    schema: {
      type: 'object',
      properties: {
        data: {
          $ref: getSchemaPath(JwtTokensDto),
        },
      },
    },
    description: '200, returns new jwt tokens',
  })
  @ApiUnauthorizedResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
      },
    },
    description: '401. Token has been expired',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError ',
  })
  @ApiBearerAuth()
  @Post('refresh-token')
  async refreshToken(
    @Body() refreshTokenDto: RefreshTokenDto,
  ): Promise<JwtTokensDto | never> {
    const decodedUser = this.jwtService.decode(
      refreshTokenDto.refreshToken,
    ) as DecodedUser

    if (!decodedUser) {
      throw new ForbiddenException('Incorrect token')
    }

    const oldRefreshToken:
      | string
      | null = await this.authService.getRefreshTokenByEmail(decodedUser.email)

    // if the old refresh token is not equal to request refresh token then this user is unauthorized
    if (!oldRefreshToken || oldRefreshToken !== refreshTokenDto.refreshToken) {
      throw new UnauthorizedException(
        'Authentication credentials were missing or incorrect',
      )
    }

    const payload = {
      id: decodedUser.id,
      email: decodedUser.email,
    }

    return this.authService.signin(payload)
  }

  @ApiNoContentResponse({
    description: 'No content. 204',
  })
  @ApiForbiddenResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        error: 'Invalid Token Signature',
      },
    },
    description: 'Invalid Token',
  })
  @ApiNotFoundResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        error: 'Not Found',
      },
    },
    description: 'User was not found',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Get('verify/:token')
  async verifyUser(@Param('token') token: string): Promise<any | never> {
    let verified
    try {
      verified = await this.authService.verifyEmailVerToken(
        token,
        authConstants.jwt.secrets.accessToken,
      )
    } catch (error) {
      // TODO: redirect to forbiden page on frontend/render template
      throw new ForbiddenException(error)
    }

    const foundUser = await this.userService.findOne(verified.id)

    if (!foundUser) {
      throw new NotFoundException('The user does not exist')
    }

    // TODO: redirect to verified page on frontend/render template
    return this.userService.markAsVerified(foundUser.id, true)
  }

  @ApiNoContentResponse({
    description: 'no content',
  })
  @ApiUnauthorizedResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
      },
    },
    description: 'Token has been expired',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: 'InternalServerError',
  })
  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Delete('logout/:token')
  @HttpCode(HttpStatus.NO_CONTENT)
  async logout(@Param('token') token: string): Promise<any | never> {
    const decodedUser: DecodedUser | null = await this.authService.verifyToken(
      token,
      authConstants.jwt.secrets.accessToken,
    )

    if (!decodedUser) {
      throw new ForbiddenException('Incorrect token')
    }

    const deletedUsersCount = await this.authService.deleteTokenByEmail(
      decodedUser.email,
    )

    if (deletedUsersCount === 0) {
      throw new NotFoundException()
    }

    return {}
  }

  @ApiNoContentResponse({
    description: 'no content',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiBearerAuth()
  @Delete('logout-all')
  @UseGuards(RolesGuard)
  @Roles(RolesEnum.admin)
  @HttpCode(HttpStatus.NO_CONTENT)
  async logoutAll(): Promise<any> {
    return this.authService.deleteAllTokens()
  }

  @ApiOkResponse({
    type: User,
    description: '200, returns a decoded user from access token',
  })
  @ApiUnauthorizedResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
      },
    },
    description: '403, says you Unauthorized',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Get('token')
  async getUserByAccessToken(
    @AuthBearer() token: string,
  ): Promise<DecodedUser | never> {
    const decodedUser: DecodedUser | null = await this.authService.verifyToken(
      token,
      authConstants.jwt.secrets.accessToken,
    )

    if (!decodedUser) {
      throw new ForbiddenException('Incorrect token')
    }

    const { exp, iat, ...user } = decodedUser

    return user
  }
}
