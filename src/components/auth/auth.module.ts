import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import LocalStrategy from './strategies/local.strategy'
import AuthRepository from './auth.repository'
import JwtAccessStrategy from './strategies/jwt-access.strategy'
import JwtRefreshStrategy from './strategies/jwt-refresh.strategy'

@Module({
  imports: [
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
    }),
  ],
  providers: [
    AuthService, 
    JwtAccessStrategy,
    JwtRefreshStrategy,
    LocalStrategy,
    AuthRepository,
  ],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}