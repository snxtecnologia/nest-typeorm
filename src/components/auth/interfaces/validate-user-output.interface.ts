import { RolesEnum } from '../../../decorators/roles.decorator'

export interface ValidateUserOutput {
  id: number;
  name?: string
  email?: string;
  role?: number;
}
