export interface UserPayload {
  readonly id: number;

  readonly email: string;

  readonly role: string;
}
