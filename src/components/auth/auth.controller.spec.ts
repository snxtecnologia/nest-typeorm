import { Test, TestingModule } from '@nestjs/testing';

import { JwtService } from '@nestjs/jwt';
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { UserService } from '../user/user.service'

describe('Auth Controller', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        { provide: AuthService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: JwtService, useValue: {} },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should have signIn Method', () => {
    expect(controller.signIn).toBeDefined();
  });

  // it('should return an array of cats', async () => {
  //   const user: SignInDto = {email: 'test@mail.com', password: 'testp@ss'}
  //   const tokens: JwtTokensDto = {accessToken: 'test', refreshToken: 'test'}
  //   jest.spyOn(controller, 'signIn').mockImplementation(() => Promise.resolve(tokens));

  //   expect(await controller.signIn()).toBe(tokens);
  // });


  
});
