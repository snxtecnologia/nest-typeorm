import { Get, Post, Delete, Body, Controller, Param, Put, UseGuards, Query } from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import JwtAccessGuard from '../../guards/jwt-access.guard'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { UserService } from './user.service'
import { PaginatedRequest } from '../../interfaces/paginated-request.interface'
import { ChangePasswordDto } from './dto/change-password.dto copy'

@ApiTags('Users Management')
@Controller('api/users')
export class UserController {

  constructor(private readonly userService: UserService) { }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Post()
  async create(@Body() user: CreateUserDto) {
    return await this.userService.create(user)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Get('paginate')
  paginate(@Query() query: PaginatedRequest) {
    return this.userService.paginate(query)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Get(':id')
  findById(@Param('id') id: string) {
    return this.userService.findOne(+id)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Get(':email')
  findByEmail(@Param('email') email: string) {
    return this.userService.findByEmail(email)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(+id, updateUserDto)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Put('change-password/:id')
  changePassword(@Param('id') id: string, @Body() changepPasswordDto: ChangePasswordDto) {
    return this.userService.changePassword(+id, changepPasswordDto)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    // return this.userService.remove(+id);
  }

}
