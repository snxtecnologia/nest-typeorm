import { Injectable } from '@nestjs/common'
import { User } from './entities/user.entity'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { Repository, UpdateResult } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { PaginatedRequest } from '../../interfaces/paginated-request.interface'
import { PaginatedResponse } from '../../interfaces/paginated-response.interface'
import { ChangePasswordDto } from './dto/change-password.dto copy'
import { hashSync, compareSync } from 'bcrypt'
import { JwtService } from '@nestjs/jwt'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) { }

  async create(user: CreateUserDto): Promise<User> {
    const hashedPassword = this.encryptPassword(user.password)

    return await this.userRepository.save({
      ...user,
      password: hashedPassword,
    })
  }

  async paginate(query: PaginatedRequest): Promise<PaginatedResponse<User>> {
    const metadata = Object.assign(new PaginatedRequest(), query)

      metadata.count = await this.userRepository.count()
      metadata.totalPages = Math.floor(metadata.count / +query.limit)

      const items: User[] = await this.userRepository.find({
        skip: +query.offset * +query.limit,
        take: +query.limit,
        order: query.order,
        // loadRelationIds: { relations: ['users'] },
      })

      return { items, metadata }
  }

  public async findAll(): Promise<User[] | null> {
    return await this.userRepository.find()
  }

  public async findOne(id: number): Promise<User | null> {
    return await this.userRepository.findOne({
      where: { id }
    })
  }

  async findByEmail(email: string): Promise<User> {
    return await this.userRepository.findOne({
      where: { email }
    })
  }

  async findByToken(token: string): Promise<User> {
    const opts: any = this.jwtService.decode(token)
    // console.log(opts)
    return await this.userRepository.findOne({
      where: { id: opts.id }
    })
    return 
  }

  async update(id: number, user: UpdateUserDto): Promise<UpdateResult | null> {
    return await this.userRepository.update(id, user)
  }

  async changePassword(id: number, user: ChangePasswordDto): Promise<UpdateResult | null> {
    const hashedPassword = this.encryptPassword(user.password)

    return await this.userRepository.update(id, {
      password: hashedPassword,
    })
  }

  async markAsVerified(id: number, verified: boolean): Promise<UpdateResult | null> {
    return await this.userRepository.update(id, { verified })
  }

  encryptPassword (password: string): string {
    // const salt = genSaltSync(10)
    const encrypted = hashSync(password, 8)

    return encrypted
  }

  validatePassword (password: string, encrypted: string): boolean {
    return compareSync(password, encrypted)
  }
}
