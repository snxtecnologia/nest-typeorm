import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'varchar', nullable: false })
  name: string

  @Column({ type: 'varchar', nullable: false, unique: true })
  email: string

  @Column({ type: 'varchar', nullable: false })
  password: string

  @Column("varchar", { name: "phone", nullable: true })
  phone: string | null

  @Column({ type: 'boolean', nullable: false })
  active: boolean

  @Column({ type: 'integer', nullable: false })
  role: number

  @Column({ type: 'boolean', default: false, nullable: false })
  verified = false

  @Column("date", { name: "createdAt", default: () => "now()" })
  createdAt: string

  @Column("date", { name: "updatedAt", default: () => "now()" })
  updatedAt: string
}
