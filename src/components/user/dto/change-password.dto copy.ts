import { ApiProperty, PartialType } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'
import { CreateUserDto } from './create-user.dto'

export class ChangePasswordDto extends PartialType(CreateUserDto) {
  
  @ApiProperty()
  @IsNotEmpty()
  password: string

}
