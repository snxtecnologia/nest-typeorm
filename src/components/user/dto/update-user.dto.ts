import { ApiProperty, PartialType } from '@nestjs/swagger'
import { Transform } from 'class-transformer'
import { IsNotEmpty, IsEmail, Validate, IsBoolean } from 'class-validator'
import { Not } from 'typeorm'
import { Unique } from '../../../validators/unique.validator'
import { User } from '../entities/user.entity'
import { CreateUserDto } from './create-user.dto'

export class UpdateUserDto extends PartialType(CreateUserDto) {
  
  // id: number

  @ApiProperty()
  @IsNotEmpty()
  name?: string
  
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @Validate(Unique, [User, ({ email, id }) => ({ id: Not(id), email })])
  @Transform((email) => email.value.toLowerCase())
  email: string

  @ApiProperty()
  @IsNotEmpty()
  role: number


}
