import { Optional } from '@nestjs/common'
import { ApiProperty, ApiQuery } from '@nestjs/swagger'
import { Transform } from 'class-transformer'
import { IsNotEmpty, IsEmail, Validate, Min, Max, IsInt } from 'class-validator'
import { Unique } from '../../../validators/unique.validator'
import { User } from '../entities/user.entity'

export class CreateUserDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string
  
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @Validate(Unique, [User])
  @Transform((email) => email.value.toLowerCase())
  email: string

  @ApiProperty()
  @IsNotEmpty()
  password: string

  @ApiProperty()
  @Optional()
  phone: string

  @ApiProperty({
    example: true
  })
  @IsNotEmpty()
  active = true

  @ApiProperty({
    example: true
  })
  @IsNotEmpty()
  verified = true

  @ApiProperty({
    example: 1
  })
  @IsNotEmpty()
  @Min(1)
  @IsInt()
  role: number
}
