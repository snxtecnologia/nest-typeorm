import { ValidationPipe, PreconditionFailedException } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { useContainer } from 'class-validator'
import { AppModule } from './components/app/app.module'
import { ContextInterceptor } from './interceptors/context.interceptor'
import * as bodParser from 'body-parser'

async function bootstrap() {
  // Instantiate app module
  const app = await NestFactory.create(AppModule)
  const port = process.env.SERVER_PORT || 3000

  app.use(bodParser.json({limit: '50mb'}))
  app.use(bodParser.urlencoded({limit: '50mb', extended: true}))
  app.enableCors()

  app.useGlobalInterceptors(new ContextInterceptor())

  // Use class-validator globally with app container
  useContainer(app.select(AppModule), { fallbackOnErrors: true })

  // Handle 
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (errors) => new PreconditionFailedException(errors),
      validationError: {
        target: false
      }
    }),
  )

  const options = new DocumentBuilder()
    .setTitle('API')
    .setDescription('API Description')
    // .setExternalDoc("Github WhatsApp HTTP API", "https://github.com/...")
    .setVersion('0.1')
    // .addTag('', '')
    .addBearerAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)

  SwaggerModule.setup('api', app, document)

  await app.listen(port, async () => {
    console.log(`The server is running on ${port} port: http://localhost:${port}/api`)
  })
}
bootstrap()
