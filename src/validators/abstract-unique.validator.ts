import { ValidationArguments, ValidatorConstraintInterface } from 'class-validator'
import { Connection } from 'typeorm'

export abstract class UniqueValidator implements ValidatorConstraintInterface {
  protected constructor(
    protected readonly connection: Connection,
    // protected readonly request: Request
  ) { }

  public async validate(value: string, args: ValidationArguments) {
    // console.log(args.object)
    const [EntityClass, findCondition = args.property] = args.constraints
    return (
      (await this.connection.getRepository(EntityClass).count({
        where:
          typeof findCondition === 'function'
            ? findCondition(args.object)
            : {
              [findCondition || args.property]: value.toLowerCase(),
            },
      })) <= 0
    )
  }

  public defaultMessage(args: ValidationArguments) {
    const [EntityClass] = args.constraints
    const entity = EntityClass.name || 'Entity'
    return `${entity} with the same '${args.property}' already exist`
  }
}