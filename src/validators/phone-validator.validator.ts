import { Injectable } from '@nestjs/common'
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator'

@ValidatorConstraint({ name: 'phoneValidator' })
@Injectable()
export class PhoneValidator implements ValidatorConstraintInterface {
  
  public validate(value: string) {
    const rule = /^(?:[14689][1-9]|2[12478]|3[1234578]|5[1345]|7[134579])(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$/gm
    return rule.test(value)
  }

  public defaultMessage() {
    return 'Invalid Brazilian Phone Number'
  }
}